/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cluster.wallpapers;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class wallpaper extends Activity implements AdapterView.OnItemSelectedListener,
        OnClickListener {

    private Gallery mGallery;
    private ImageView mImageView;
    private TextView mInfoView;
    private boolean mIsWallpaperSet;
    private ImageView mPreview;
    private Button mSet;
    private Button mBPreview;
    private View mDivider;
    private LinearLayout mLayout;

    private Bitmap mBitmap;
    private boolean mFullScreen = false;

    private ArrayList<Integer> mThumbs;
    private ArrayList<Integer> mImages;
    private WallpaperLoader mLoader;
    
    private static final int MENU_EXIT = Menu.FIRST;
    
    public void onBackPressed () 
    {
        return;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        findWallpapers();

        setContentView(R.layout.wallpaper_chooser);
        
        Toast.makeText(this,(getString(R.string.toast)), Toast.LENGTH_LONG).show();

        mGallery = (Gallery) findViewById(R.id.gallery);
        mGallery.setAdapter(new ImageAdapter(this));
        mGallery.setOnItemSelectedListener(this);
        mGallery.setCallbackDuringFling(false);
        
        ActionBar actionBar = getActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.set).setOnClickListener(this);

        mImageView = (ImageView) findViewById(R.id.wallpaper);
        mInfoView = (TextView) findViewById(R.id.info);
        mPreview = (ImageView) findViewById(R.id.wallpaper_preview);
        mSet = (Button) findViewById(R.id.set);
        mBPreview = (Button) findViewById(R.id.preview);
        //mDivider = (View) findViewById(R.id.divider);
        mLayout = (LinearLayout) findViewById(R.id.LinearLayout);
        
        Button preview = (Button) findViewById(R.id.preview);
	    preview.setOnClickListener(new OnClickListener()
	    {
	        public void onClick(View v)
	        {
	        	mInfoView.setVisibility(View.INVISIBLE);
	        	mSet.setVisibility(View.INVISIBLE);
	        	mGallery.setVisibility(View.INVISIBLE);
	        	mPreview.setVisibility(View.VISIBLE);
	        	mBPreview.setVisibility(View.INVISIBLE);
	        	//mDivider.setVisibility(View.INVISIBLE);
	        	mLayout.setVisibility(View.INVISIBLE);
	        	//overridePendingTransition(android.R.anim.lock_screen_wallpaper_behind_enter, android.R.anim.lock_screen_exit);
	        	getActionBar().hide();
	        	
	        	if(mFullScreen){
            		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            	}else{
            		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            				WindowManager.LayoutParams.FLAG_FULLSCREEN);
            	}
            	mFullScreen = !mFullScreen;
	        }
	    });
	    
	    ImageView previewimg = (ImageView) findViewById(R.id.wallpaper_preview);
	    previewimg.setOnClickListener(new OnClickListener()
	    {
	        public void onClick(View v)
	        {
	        	mInfoView.setVisibility(View.VISIBLE);
	        	mSet.setVisibility(View.VISIBLE);
	        	mGallery.setVisibility(View.VISIBLE);
	        	mPreview.setVisibility(View.INVISIBLE);
	        	mBPreview.setVisibility(View.VISIBLE);
	        	//mDivider.setVisibility(View.VISIBLE);
	        	mLayout.setVisibility(View.VISIBLE);
	        	getActionBar().show();
	        	if(mFullScreen){
            		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            	}else{
            		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            				WindowManager.LayoutParams.FLAG_FULLSCREEN);
            	}
            	mFullScreen = !mFullScreen;
	        }
	    });
    }

    private void setDisplayHomeAsUpEnabled() {
    }
    private void findWallpapers() {
        mThumbs = new ArrayList<Integer>(24);
        mImages = new ArrayList<Integer>(24);

        final Resources resources = getResources();
        final String packageName = getApplication().getPackageName();

        addWallpapers(resources, packageName, R.array.wallpapers);
        addWallpapers(resources, packageName, R.array.extra_wallpapers);
    }

    private void addWallpapers(Resources resources, String packageName, int list) {
        final String[] extras = resources.getStringArray(list);
        for (String extra : extras) {
            int res = resources.getIdentifier(extra, "drawable", packageName);
            if (res != 0) {
                final int thumbRes = resources.getIdentifier(extra + "_small",
                        "drawable", packageName);

                if (thumbRes != 0) {
                    mThumbs.add(thumbRes);
                    mImages.add(res);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsWallpaperSet = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        if (mLoader != null && mLoader.getStatus() != WallpaperLoader.Status.FINISHED) {
            mLoader.cancel(true);
            mLoader = null;
        }
    }

    public void onItemSelected(AdapterView parent, View v, int position, long id) {
        if (mLoader != null && mLoader.getStatus() != WallpaperLoader.Status.FINISHED) {
            mLoader.cancel();
        }
        mLoader = (WallpaperLoader) new WallpaperLoader().execute(position);
    }

    /*
     * When using touch if you tap an image it triggers both the onItemClick and
     * the onTouchEvent causing the wallpaper to be set twice. Ensure we only
     * set the wallpaper once.
     */
    private void selectWallpaper(int position) {
        if (mIsWallpaperSet) {
            return;
        }

        mIsWallpaperSet = true;
        try {
            InputStream stream = getResources().openRawResource(mImages.get(position));
            setWallpaper(stream);
            setResult(RESULT_OK);
            finish();
        } catch (IOException e) {
            Log.e("Paperless System", "Failed to set wallpaper: " + e);
        }
    }

    public void onNothingSelected(AdapterView parent) {
    }

    private class ImageAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;

        ImageAdapter(wallpaper context) {
            mLayoutInflater = context.getLayoutInflater();
        }

        public int getCount() {
            return mThumbs.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView image;

            if (convertView == null) {
                image = (ImageView) mLayoutInflater.inflate(R.layout.wallpaper_item, parent, false);
            } else {
                image = (ImageView) convertView;
            }
            
            int thumbRes = mThumbs.get(position);
            image.setImageResource(thumbRes);
            Drawable thumbDrawable = image.getDrawable();
            if (thumbDrawable != null) {
                thumbDrawable.setDither(true);
            } else {
                Log.e("Paperless System", String.format(
                    "Error decoding thumbnail resId=%d for wallpaper #%d",
                    thumbRes, position));
            }
            return image;
        }
    }
    
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, MENU_EXIT, 0, R.string.menu_exit)
        .setIcon(R.drawable.ic_menu_exit)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return super.onCreateOptionsMenu(menu);
        
	}
    
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case MENU_EXIT:    	
            	finish();     
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClick(View v) {
        selectWallpaper(mGallery.getSelectedItemPosition());
    }

    class WallpaperLoader extends AsyncTask<Integer, Void, Bitmap> {
        BitmapFactory.Options mOptions;

        WallpaperLoader() {
            mOptions = new BitmapFactory.Options();
            mOptions.inDither = false;
            mOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;            
        }
        
        protected Bitmap doInBackground(Integer... params) {
            if (isCancelled()) return null;
            try {
                return BitmapFactory.decodeResource(getResources(),
                        mImages.get(params[0]), mOptions);
            } catch (OutOfMemoryError e) {
                return null;
            }            
        }

        @Override
        protected void onPostExecute(Bitmap b) {
            if (b == null) return;

            if (!isCancelled() && !mOptions.mCancel) {
                // Help the GC
                if (mBitmap != null) {
                    mBitmap.recycle();
                }

                mInfoView.setText(getResources().getStringArray(R.array.info)[mGallery.getSelectedItemPosition()]);

                final ImageView view = mImageView;
                view.setImageBitmap(b);

                mBitmap = b;

                final Drawable drawable = view.getDrawable();
                drawable.setFilterBitmap(true);
                drawable.setDither(true);

                view.postInvalidate();

                mLoader = null;
            } else {
               b.recycle(); 
            }
        }

        void cancel() {
            mOptions.requestCancelDecode();
            super.cancel(true);
        }
    }
}